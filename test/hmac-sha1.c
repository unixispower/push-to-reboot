/*
 * Tests for hmac-sha1.c .
 */

#include "test.h"

#include "hmac-sha1.h"

#include <stdint.h>
#include <string.h>

#define KEY_SIZE 128
#define MESSAGE_SIZE 128


/* A key, message, and correct digest. */
typedef struct test_case {
    const uint8_t key[KEY_SIZE];
    const size_t key_length;
    const uint8_t message[MESSAGE_SIZE];
    const size_t message_length;
    const uint8_t digest[HMAC_SHA1_HASH_SIZE];
} test_case;

/* Test cases from RFC 2202 https://tools.ietf.org/html/rfc2202 . */
const test_case test_cases[] = {
    {
        // case 1
        .key = {
            0x0B, 0x0B, 0x0B, 0x0B, 0x0B, 0x0B, 0x0B, 0x0B, 0x0B, 0x0B,
            0x0B, 0x0B, 0x0B, 0x0B, 0x0B, 0x0B, 0x0B, 0x0B, 0x0B, 0x0B
        },
        .key_length = 20,
        .message = "Hi There",
        .message_length = 8,
        .digest = {
            0xB6, 0x17, 0x31, 0x86, 0x55, 0x05, 0x72, 0x64, 0xE2, 0x8B, 
            0xC0, 0xB6, 0xFB, 0x37, 0x8C, 0x8E, 0xF1, 0x46, 0xBE, 0x00
        }
    }, {
        // case 2
        .key = "Jefe",
        .key_length = 4,
        .message = "what do ya want for nothing?",
        .message_length = 28,
        .digest = {
            0xEF, 0xFC, 0xDF, 0x6A, 0xE5, 0xEB, 0x2F, 0xA2, 0xD2, 0x74, 
            0x16, 0xD5, 0xF1, 0x84, 0xDF, 0x9C, 0x25, 0x9A, 0x7C, 0x79
        }
    }, {
        // case 3
        .key = {
            0xAA, 0xAA, 0xAA, 0xAA, 0xAA, 0xAA, 0xAA, 0xAA, 0xAA, 0xAA, 
            0xAA, 0xAA, 0xAA, 0xAA, 0xAA, 0xAA, 0xAA, 0xAA, 0xAA, 0xAA
        },
        .key_length = 20,
        .message = {
            0xDD, 0xDD, 0xDD, 0xDD, 0xDD, 0xDD, 0xDD, 0xDD, 0xDD, 0xDD, 
            0xDD, 0xDD, 0xDD, 0xDD, 0xDD, 0xDD, 0xDD, 0xDD, 0xDD, 0xDD,
            0xDD, 0xDD, 0xDD, 0xDD, 0xDD, 0xDD, 0xDD, 0xDD, 0xDD, 0xDD,
            0xDD, 0xDD, 0xDD, 0xDD, 0xDD, 0xDD, 0xDD, 0xDD, 0xDD, 0xDD,
            0xDD, 0xDD, 0xDD, 0xDD, 0xDD, 0xDD, 0xDD, 0xDD, 0xDD, 0xDD
        },
        .message_length = 50,
        .digest = {
            0x12, 0x5D, 0x73, 0x42, 0xB9, 0xAC, 0x11, 0xCD, 0x91, 0xA3, 
            0x9A, 0xF4, 0x8A, 0xA1, 0x7B, 0x4F, 0x63, 0xF1, 0x75, 0xD3
        }
    }, {
        // case 4
        .key = {
            0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x0A, 
            0x0B, 0x0C, 0x0D, 0x0E, 0x0F, 0x10, 0x11, 0x12, 0x13, 0x14,
            0x15, 0x16, 0x17, 0x18, 0x19
        },
        .key_length = 25,
        .message = {
            0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 
            0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 0xCD,
            0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 0xCD,
            0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 0xCD,
            0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 0xCD, 0xCD
        },
        .message_length = 50,
        .digest = {
            0x4C, 0x90, 0x07, 0xF4, 0x02, 0x62, 0x50, 0xC6, 0xBC, 0x84, 
            0x14, 0xF9, 0xBF, 0x50, 0xC8, 0x6C, 0x2D, 0x72, 0x35, 0xDA
        }
    }, {
        // case 5 (without truncation)
        .key = {
            0x0C, 0x0C, 0x0C, 0x0C, 0x0C, 0x0C, 0x0C, 0x0C, 0x0C, 0x0C, 
            0x0C, 0x0C, 0x0C, 0x0C, 0x0C, 0x0C, 0x0C, 0x0C, 0x0C, 0x0C
        },
        .key_length = 20,
        .message = "Test With Truncation",
        .message_length = 20,
        .digest = {
            0x4C, 0x1A, 0x03, 0x42, 0x4B, 0x55, 0xE0, 0x7F, 0xE7, 0xF2, 
            0x7B, 0xE1, 0xD5, 0x8B, 0xB9, 0x32, 0x4A, 0x9A, 0x5A, 0x04
        }
    }, {
        // case 6
        .key = {
            0xAA, 0xAA, 0xAA, 0xAA, 0xAA, 0xAA, 0xAA, 0xAA, 0xAA, 0xAA, 
            0xAA, 0xAA, 0xAA, 0xAA, 0xAA, 0xAA, 0xAA, 0xAA, 0xAA, 0xAA,
            0xAA, 0xAA, 0xAA, 0xAA, 0xAA, 0xAA, 0xAA, 0xAA, 0xAA, 0xAA,
            0xAA, 0xAA, 0xAA, 0xAA, 0xAA, 0xAA, 0xAA, 0xAA, 0xAA, 0xAA,
            0xAA, 0xAA, 0xAA, 0xAA, 0xAA, 0xAA, 0xAA, 0xAA, 0xAA, 0xAA,
            0xAA, 0xAA, 0xAA, 0xAA, 0xAA, 0xAA, 0xAA, 0xAA, 0xAA, 0xAA,
            0xAA, 0xAA, 0xAA, 0xAA, 0xAA, 0xAA, 0xAA, 0xAA, 0xAA, 0xAA, 
            0xAA, 0xAA, 0xAA, 0xAA, 0xAA, 0xAA, 0xAA, 0xAA, 0xAA, 0xAA
        },
        .key_length = 80,
        .message = "Test Using Larger Than Block-Size Key - Hash Key First",
        .message_length = 54,
        .digest = {
            0xAA, 0x4A, 0xE5, 0xE1, 0x52, 0x72, 0xD0, 0x0E, 0x95, 0x70, 
            0x56, 0x37, 0xCE, 0x8A, 0x3B, 0x55, 0xED, 0x40, 0x21, 0x12
        }
    }, {
        // case 7
        .key = {
            0xAA, 0xAA, 0xAA, 0xAA, 0xAA, 0xAA, 0xAA, 0xAA, 0xAA, 0xAA, 
            0xAA, 0xAA, 0xAA, 0xAA, 0xAA, 0xAA, 0xAA, 0xAA, 0xAA, 0xAA,
            0xAA, 0xAA, 0xAA, 0xAA, 0xAA, 0xAA, 0xAA, 0xAA, 0xAA, 0xAA,
            0xAA, 0xAA, 0xAA, 0xAA, 0xAA, 0xAA, 0xAA, 0xAA, 0xAA, 0xAA,
            0xAA, 0xAA, 0xAA, 0xAA, 0xAA, 0xAA, 0xAA, 0xAA, 0xAA, 0xAA,
            0xAA, 0xAA, 0xAA, 0xAA, 0xAA, 0xAA, 0xAA, 0xAA, 0xAA, 0xAA,
            0xAA, 0xAA, 0xAA, 0xAA, 0xAA, 0xAA, 0xAA, 0xAA, 0xAA, 0xAA, 
            0xAA, 0xAA, 0xAA, 0xAA, 0xAA, 0xAA, 0xAA, 0xAA, 0xAA, 0xAA
        },
        .key_length = 80,
        .message =
            "Test Using Larger Than Block-Size Key and Larger "
            "Than One Block-Size Data",
        .message_length = 73,
        .digest = {
            0xE8, 0xE9, 0x9D, 0x0F, 0x45, 0x23, 0x7D, 0x78, 0x6D, 0x6B, 
            0xBA, 0xA7, 0x96, 0x5C, 0x78, 0x08, 0xBB, 0xFF, 0x1A, 0x91
        }
    }
};

/* Hash each test message and compare to known correct hash value. */
void test_all_cases(const test_case *test_cases, size_t length) {
    for (size_t i = 0; i < length; ++i) {
        uint8_t digest[SHA1_HASH_SIZE];

        hmac_sha1_hash(
            test_cases[i].key, test_cases[i].key_length,
            test_cases[i].message, test_cases[i].message_length,
            digest);

        TEST_ASSERT_ALL(
            digest, ==, test_cases[i].digest, SHA1_HASH_SIZE,
            "Message in test case %zu hashes to incorrect value.", i + 1);
    }
}

/* Test example 1 provided in module documentation. */
void test_single_part_message(void) {
    const uint8_t actual_digest[SHA1_HASH_SIZE] = {
        0xB3, 0x1B, 0x75, 0x92, 0x90, 0x87, 0x25, 0x7D, 0xC1, 0x83, 
        0x9D, 0xBC, 0x1C, 0xF9, 0xE3, 0x6B, 0xE4, 0x6E, 0x59, 0x46
    };

    const uint8_t key[] = "password123";
    const size_t key_length = 11;
    const uint8_t message[] = "This is the message.";
    const size_t message_length = 20;
    uint8_t digest[SHA1_HASH_SIZE];

    hmac_sha1_hash(key, key_length, message, message_length, digest);

    TEST_ASSERT_ALL(
        digest, ==, actual_digest, SHA1_HASH_SIZE,
        "Documentation example 1 hashes to incorrect value.");
}

/* Test example 2 provided in module documentation. */
void test_multi_part_message(void) {
    const uint8_t actual_digest[SHA1_HASH_SIZE] = {
        0xB3, 0x1B, 0x75, 0x92, 0x90, 0x87, 0x25, 0x7D, 0xC1, 0x83, 
        0x9D, 0xBC, 0x1C, 0xF9, 0xE3, 0x6B, 0xE4, 0x6E, 0x59, 0x46
    };

    const uint8_t key[] = "password123";
    const size_t key_length = 11;
    const uint8_t message_a[] = "This is ";
    const size_t message_a_length = 8;
    const uint8_t message_b[] = "the message.";
    const size_t message_b_length = 12;
    hmac_sha1_context context;
    uint8_t digest[SHA1_HASH_SIZE];

    hmac_sha1_init(&context, key, key_length);
    hmac_sha1_update(&context, message_a, message_a_length);
    hmac_sha1_update(&context, message_b, message_b_length);
    hmac_sha1_digest(&context, digest);

    TEST_ASSERT_ALL(
        digest, ==, actual_digest, SHA1_HASH_SIZE,
        "Documentation example 2 hashes to incorrect value.");
}

int main(void) {
    test_all_cases(test_cases, sizeof(test_cases)/sizeof(test_cases[0]));
    test_single_part_message();
    test_multi_part_message();

    return 0;
}

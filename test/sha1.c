/*
 * Tests for sha1.c .
 */

#include "sha1.h"

#include "test.h"

#include <stdint.h>
#include <string.h>

#define MESSAGE_SIZE 128


/* A message and its correct digest. */
typedef struct test_case {
    const uint8_t message[MESSAGE_SIZE];
    const size_t message_length;
    const uint8_t digest[SHA1_HASH_SIZE];
} test_case;

/* Test cases from https://www.di-mgt.com.au/sha_testvectors.html . */
const test_case test_cases[] = {
    {
        // case 1
        .message = { 0 },
        .message_length = 0,
        .digest = {
            0xDA, 0x39, 0xA3, 0xEE, 0x5E, 0x6B, 0x4B, 0x0D, 0x32, 0x55, 
            0xBF, 0xEF, 0x95, 0x60, 0x18, 0x90, 0xAF, 0xD8, 0x07, 0x09
        }
    }, {
        // case 2
        .message = "abc",
        .message_length = 3,
        .digest = {
            0xA9, 0x99, 0x3E, 0x36, 0x47, 0x06, 0x81, 0x6A, 0xBA, 0x3E,
            0x25, 0x71, 0x78, 0x50, 0xC2, 0x6C, 0x9C, 0xD0, 0xD8, 0x9D
        }
    }, {
        // case 3
        .message = "abcdbcdecdefdefgefghfghighijhijkijkljklmklmnlmnomnopnopq",
        .message_length = 56,
        .digest = {
            0x84, 0x98, 0x3E, 0x44, 0x1C, 0x3B, 0xD2, 0x6E, 0xBA, 0xAE, 
            0x4A, 0xA1, 0xF9, 0x51, 0x29, 0xE5, 0xE5, 0x46, 0x70, 0xF1
        }
    }, {
        // case 4
        .message =
            "abcdefghbcdefghicdefghijdefghijkefghijklfghijklmghijklmn"
            "hijklmnoijklmnopjklmnopqklmnopqrlmnopqrsmnopqrstnopqrstu",
        .message_length = 112,
        .digest = {
            0xA4, 0x9B, 0x24, 0x46, 0xA0, 0x2C, 0x64, 0x5B, 0xF4, 0x19, 
            0xF9, 0x95, 0xB6, 0x70, 0x91, 0x25, 0x3A, 0x04, 0xA2, 0x59
        }
    }
};

/* Hash each test message and compare to known correct hash value. */
void test_all_cases(const test_case *test_cases, size_t length) {
    for (size_t i = 0; i < length; ++i) {
        uint8_t digest[SHA1_HASH_SIZE];

        sha1_hash(test_cases[i].message, test_cases[i].message_length, digest);

        TEST_ASSERT_ALL(
            digest, ==, test_cases[i].digest, SHA1_HASH_SIZE,
            "Message in test case %zu hashes to incorrect value.", i + 1);
    }
}

/* Test example 1 provided in module documentation. */
void test_single_part_message(void) {
    const uint8_t actual_digest[SHA1_HASH_SIZE] = {
        0xF8, 0xEA, 0x75, 0x31, 0xA2, 0xAD, 0xC1, 0x0C, 0xFD, 0xDD, 
        0x88, 0xFC, 0xE5, 0x3B, 0x06, 0xC8, 0xBD, 0x98, 0x3A, 0x19
    };

    const uint8_t message[] = "This is the message.";
    const size_t message_length = 20;
    uint8_t digest[SHA1_HASH_SIZE];

    sha1_hash(message, message_length, digest);

    TEST_ASSERT_ALL(
        digest, ==, actual_digest, SHA1_HASH_SIZE,
        "Documentation example 1 hashes to incorrect value.");
}

/* Test example 2 provided in module documentation. */
void test_multi_part_message(void) {
    const uint8_t actual_digest[SHA1_HASH_SIZE] = {
        0xF8, 0xEA, 0x75, 0x31, 0xA2, 0xAD, 0xC1, 0x0C, 0xFD, 0xDD, 
        0x88, 0xFC, 0xE5, 0x3B, 0x06, 0xC8, 0xBD, 0x98, 0x3A, 0x19
    };

    const uint8_t message_a[] = "This is ";
    const size_t message_a_length = 8;
    const uint8_t message_b[] = "the message.";
    const size_t message_b_length = 12;
    sha1_context context;
    uint8_t digest[SHA1_HASH_SIZE];

    sha1_init(&context);
    sha1_update(&context, message_a, message_a_length);
    sha1_update(&context, message_b, message_b_length);
    sha1_digest(&context, digest);

    TEST_ASSERT_ALL(
        digest, ==, actual_digest, SHA1_HASH_SIZE,
        "Documentation example 2 hashes to incorrect value.");
}

int main(void) {
    test_all_cases(test_cases, sizeof(test_cases)/sizeof(test_cases[0]));
    test_single_part_message();
    test_multi_part_message();

    return 0;
}

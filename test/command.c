/*
 * Tests for command.c .
 */

#include "command.h"

#include "test.h"

#include <stdbool.h>


/* Test that command with invalid node number does not parse. */
void test_invalid_node_number(void) {
    const char *string = "A1A";
    command_info command;
    bool success;

    success = command_parse(&command, string);

    TEST_ASSERT(success, ==, false, "Invalid node number not rejected.");
}

/* Test that command with invalid port number does not parse. */
void test_invalid_port_number(void) {
    const char *string = "1AA";
    command_info command;
    bool success;

    success = command_parse(&command, string);

    TEST_ASSERT(success, ==, false, "Invalid port number not rejected.");
}

/* Test that command with invalid command does not parse. */
void test_invalid_command(void) {
    const char *string = "111";
    command_info command;
    bool success;

    success = command_parse(&command, string);

    TEST_ASSERT(success, ==, false, "Invalid command number not rejected.");
}

/* Test that valid command parses successfully. */
void test_valid_command(void) {
    const char *string = "11A";
    command_info command;
    bool success;

    success = command_parse(&command, string);

    TEST_ASSERT(success, ==, true, "Valid command was rejected.");
    TEST_ASSERT(command.node_number, ==, 1, "Incorrect node number parsed.");
    TEST_ASSERT(command.port_number, ==, 1, "Incorrect port number parsed.");
    TEST_ASSERT(
        command.action, ==, COMMAND_ACTION_ACTIVATE,
        "Incorrect action parsed.");
}

/* Test that command containing non-zero node number is for single node. */
void test_destination_single_node(void) {
    command_info command = { .node_number = 1 };
    bool is_for_all_nodes;

    is_for_all_nodes = command_is_for_all_nodes(&command);

    TEST_ASSERT(
        is_for_all_nodes, ==, false,
        "Single node command recognized as multiple node command.");
}

/* Test that command containing non-zero port number is for single port. */
void test_destination_single_port(void) {
    command_info command = { .port_number = 1 };
    bool is_for_all_ports;

    is_for_all_ports = command_is_for_all_ports(&command);

    TEST_ASSERT(
        is_for_all_ports, ==, false,
        "Single port command recognized as multiple port command.");
}

/* Test that command containing zero node number is for all nodes. */
void test_destination_all_nodes(void) {
    command_info command = { .node_number = 0 };
    bool is_for_all_nodes;

    is_for_all_nodes = command_is_for_all_nodes(&command);

    TEST_ASSERT(
        is_for_all_nodes, ==, true,
        "Multiple node command recognized as single node command.");
}

/* Test that command containing zero port number is for all ports. */
void test_destination_all_ports(void) {
    command_info command = { .port_number = 0 };
    bool is_for_all_ports;

    is_for_all_ports = command_is_for_all_ports(&command);

    TEST_ASSERT(
        is_for_all_ports, ==, true,
        "Multiple port command recognized as single port command.");
}

int main(void) {
    test_invalid_node_number();
    test_invalid_port_number();
    test_invalid_command();
    test_valid_command();
    test_destination_single_node();
    test_destination_single_port();
    test_destination_all_nodes();
    test_destination_all_ports();

    return 0;
}

/*
 * Tests for base32.c .
 */

#include "base32.h"

#include "test.h"

#include <string.h>

#define STRING_SIZE 32
#define BYTES_SIZE 16


/* A base 32 string and decoded bytes. */
typedef struct test_case {
    char string[STRING_SIZE];
    uint8_t bytes[BYTES_SIZE];
    size_t bytes_length;
} test_case;

/* Test cases 1-7 from https://tools.ietf.org/html/rfc4648#page-12 . */
const test_case test_cases[] = {
    {
        // case 1
        .string = "",
        .bytes = "",
        .bytes_length = 0
    }, {
        // case 2
        .string = "MY======",
        .bytes = "f",
        .bytes_length = 1
    }, {
        // case 3
        .string = "MZXQ====",
        .bytes = "fo",
        .bytes_length = 2
    }, {
        // case 4
        .string = "MZXW6===",
        .bytes = "foo",
        .bytes_length = 3
    }, {
        // case 5
        .string = "MZXW6YQ=",
        .bytes = "foob",
        .bytes_length = 4
    }, {
        // case 6
        .string = "MZXW6YTB",
        .bytes = "fooba",
        .bytes_length = 5
    }, {
        // case 7
        .string = "MZXW6YTBOI======",
        .bytes = "foobar",
        .bytes_length = 6
    }, {
        // case 8 (not in RFC test cases)
        .string = "MZXW6YTBOJRA====",
        .bytes = "foobarb",
        .bytes_length = 7
    }, {
        // case 9 (not in RFC test cases)
        .string = "MZXW6YTBOJRGC===",
        .bytes = "foobarba",
        .bytes_length = 8
    }, {
        // case 10 (not in RFC test cases)
        .string = "MZXW6YTBOJRGC6Q=",
        .bytes = "foobarbaz",
        .bytes_length = 9
    }, {
        // case 11 (not in RFC test cases)
        .string = "MZXW6YTBOJRGC6RB",
        .bytes = "foobarbaz!",
        .bytes_length = 10
    }
};

/* Remove padding characters from a string. */
void remove_padding(char *string) {
    char *padding_position;
    size_t new_length;

    padding_position = strstr(string, "=");
    if (padding_position != NULL) {
        new_length = padding_position - string;
    } else {
        new_length = strlen(string);
    }
    string[new_length] = '\0';
}

/* Decode each test string then compare to known correct value. */
void test_all_cases(const test_case *test_cases, size_t length) {
    for (size_t i = 0; i < length; ++i) {
        uint8_t bytes[BYTES_SIZE];
        size_t byte_count;
        base32_error error;

        error = base32_decode(
            test_cases[i].string, bytes, BYTES_SIZE, &byte_count);

        TEST_ASSERT(
            error, ==, base32_error_success,
            "String in test case %zu could not be decoded.", i + 1);
        TEST_ASSERT(
            byte_count, ==, test_cases[i].bytes_length,
            "String in test case %zu decodes to incorrect length.", i + 1);
        TEST_ASSERT_ALL(
            bytes, ==, test_cases[i].bytes, byte_count,
            "String in test case %zu decodes to incorrect value.", i + 1);
    }
}

/* Truncate and decode each test string then compare to known correct value. */
void test_all_cases_without_padding(
        const test_case *test_cases, size_t length) {
    for (size_t i = 0; i < length; ++i) {
        char truncated_string[STRING_SIZE];
        uint8_t bytes[BYTES_SIZE];
        size_t byte_count;
        base32_error error;

        // remove padding from input string
        memcpy(truncated_string, test_cases[i].string, STRING_SIZE);
        remove_padding(truncated_string);

        error = base32_decode(
            truncated_string, bytes, BYTES_SIZE, &byte_count);

        TEST_ASSERT(
            error, ==, base32_error_success,
            "Truncated string in test case %zu could not be decoded.", i + 1);
        TEST_ASSERT(
            byte_count, ==, test_cases[i].bytes_length,
            "Truncated string in test case %zu decodes to incorrect length.",
            i + 1);
        TEST_ASSERT_ALL(
            bytes, ==, test_cases[i].bytes, byte_count,
            "Truncated string in test case %zu decodes to incorrect value.",
            i + 1);
    }
}

/* Test case insensitivity of decoding. */
void test_case_insensitivity() {
    const char *string = "KrSxG5dJnZTtCMrt";
    uint8_t bytes[BYTES_SIZE];
    size_t byte_count;
    base32_error error;

    error = base32_decode(string, bytes, BYTES_SIZE, &byte_count);

    TEST_ASSERT(
        error, ==, base32_error_success,
        "Mixed case string could not be decoded.");
    TEST_ASSERT_ALL(
        bytes, ==, "Testing123", byte_count,
        "Mixed case string decodes incorrectly.");
}

/* Test failure on invalid character input. */
void test_invalid_character() {
    const char *string = "ABCDEF0"; // 0 is the bad apple
    uint8_t bytes[BYTES_SIZE];
    size_t byte_count;
    base32_error error;

    error = base32_decode(string, bytes, BYTES_SIZE, &byte_count);

    TEST_ASSERT(
        error, ==, base32_error_invalid_input,
        "Invalid character did not cause decode failure.");
}

/* Test failure on attempt to overfill output buffer */
void test_output_overflow() {
    const char *string = "KRSXG5DJNZTTCMRT"; // decodes to 10 characters
    uint8_t bytes[BYTES_SIZE];
    size_t byte_count;
    base32_error error;

    error = base32_decode(string, bytes, 4, &byte_count);

    TEST_ASSERT(
        error, ==, base32_error_output_full,
        "Output buffer overflow not detected.");
}

int main(void) {
    test_all_cases(test_cases, sizeof(test_cases)/sizeof(test_cases[0]));
    test_all_cases_without_padding(
        test_cases, sizeof(test_cases)/sizeof(test_cases[0]));
    test_case_insensitivity();
    test_invalid_character();
    test_output_overflow();

    return 0;
}

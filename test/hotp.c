/*
 * Tests for hotp.c .
 */
 
#include "test.h"

#include "hotp.h"

#include <stdbool.h>
#include <stdint.h>
#include <string.h>


/* Test sequence of generated passwords using vector provided in RFC 4226 */
void test_sequence(void) {
    // See https://tools.ietf.org/html/rfc4226#page-32
    const uint8_t secret[] = "12345678901234567890";
    const size_t secret_length = 20;
    const char *passwords[] = {
        "755224", "287082", "359152", "969429", "338314",
        "254676", "287922", "162583", "399871", "520489"
    };
    hotp_context context;

    hotp_init(&context, secret, secret_length);
    for (size_t i = 0; i < sizeof(passwords) / sizeof(passwords[0]); ++i) {
        char password[HOTP_PASSWORD_LENGTH + 1];

        hotp_next(&context, password);

        TEST_ASSERT_ALL(
            password, ==, passwords[i], HOTP_PASSWORD_LENGTH,
            "Password %zu does not match expected value.", i + 1);
    }
}

/* Test that lookahead works within the specified range. */
void test_lookahead_in_range(void) {
    const uint8_t secret[] = "18267539029098340958";
    const size_t secret_length = 20;
    hotp_context context;
    char password[HOTP_PASSWORD_LENGTH + 1];
    bool found;

    // generate 1st password
    hotp_init(&context, secret, secret_length);
    hotp_next(&context, password);

    // check generated password
    hotp_init(&context, secret, secret_length);
    found = hotp_check(&context, password);

    TEST_ASSERT(found, ==, true, "Password was not found in lookahead range.");
}

/* Test that lookahead fails outside of specified range. */
void test_lookahead_out_of_range(void) {
    const uint8_t secret[] = "23098509843095803984";
    const size_t secret_length = 20;
    hotp_context context;
    char password[HOTP_PASSWORD_LENGTH + 1];
    bool found;

    // generate (HOTP_LOOKAHEAD_COUNT + 1)th password
    hotp_init(&context, secret, secret_length);
    for (size_t i = 0; i <= HOTP_LOOKAHEAD_COUNT; ++i) {
        hotp_next(&context, password);
    }

    // check generated password
    hotp_init(&context, secret, secret_length);
    found = hotp_check(&context, password);

    TEST_ASSERT(found, ==, false, "Password was found in lookahead range.");
}

/* Test that replayed passwords are rejected. */
void test_replay_rejection(void) {
    const uint8_t secret[] = "2345098t439424009345";
    const size_t secret_length = 20;
    hotp_context context;
    char password[HOTP_PASSWORD_LENGTH + 1];
    bool found;

    // generate 1st password
    hotp_init(&context, secret, secret_length);
    hotp_next(&context, password);

    // reset generator
    hotp_init(&context, secret, secret_length);

    found = hotp_check(&context, password);
    TEST_ASSERT(found, ==, true, "Password was not found in lookahead range.");

    found = hotp_check(&context, password);
    TEST_ASSERT(found, ==, false, "Replayed password was accepted.");
}

int main(void) {
    test_sequence();
    test_lookahead_in_range();
    test_lookahead_out_of_range();
    test_replay_rejection();

    return 0;
}

/*
 * Tests for hex.c .
 */

#include "hex.h"

#include "test.h"

#include <string.h>

#define STRING_SIZE 32
#define BYTES_SIZE 16


/* A hexadecimal string and decoded bytes. */
typedef struct test_case {
    char string[STRING_SIZE];
    uint8_t bytes[BYTES_SIZE];
    size_t bytes_length;
} test_case;

/* Test cases from https://tools.ietf.org/html/rfc4648#page-12 . */
const test_case test_cases[] = {
    {
        // case 1
        .string = "",
        .bytes = "",
        .bytes_length = 0
    }, {
        // case 2
        .string = "66",
        .bytes = "f",
        .bytes_length = 1
    }, {
        // case 3
        .string = "666F",
        .bytes = "fo",
        .bytes_length = 2
    }, {
        // case 4
        .string = "666F6F",
        .bytes = "foo",
        .bytes_length = 3
    }, {
        // case 5
        .string = "666F6F62",
        .bytes = "foob",
        .bytes_length = 4
    }, {
        // case 6
        .string = "666F6F6261",
        .bytes = "fooba",
        .bytes_length = 5
    }, {
        // case 7
        .string = "666F6F626172",
        .bytes = "foobar",
        .bytes_length = 6
    }
};

/* Decode each test string then compare to known correct value. */
void test_all_cases(const test_case *test_cases, size_t length) {
    for (size_t i = 0; i < length; ++i) {
        uint8_t bytes[BYTES_SIZE];
        size_t byte_count;
        hex_error error;

        error = hex_decode(
            test_cases[i].string, bytes, BYTES_SIZE, &byte_count);

        TEST_ASSERT(
            error, ==, hex_error_success,
            "String in test case %zu could not be decoded.", i + 1);
        TEST_ASSERT(
            byte_count, ==, test_cases[i].bytes_length,
            "String in test case %zu decodes to incorrect length.", i + 1);
        TEST_ASSERT_ALL(
            bytes, ==, test_cases[i].bytes, byte_count,
            "String in test case %zu decodes to incorrect value.", i + 1);
    }
}

/* Test case insensitivity of decoding. */
void test_case_insensitivity() {
    const char *string = "54455A5a5A54494e47";
    uint8_t bytes[BYTES_SIZE];
    size_t byte_count;
    hex_error error;

    error = hex_decode(string, bytes, BYTES_SIZE, &byte_count);

    TEST_ASSERT(
        error, ==, hex_error_success,
        "Mixed case string could not be decoded.");
    TEST_ASSERT_ALL(
        bytes, ==, "TEZZZTING", byte_count,
        "Mixed case string decodes incorrectly.");
}

/* Test failure on invalid character input. */
void test_invalid_character() {
    const char *string = "ABCDEZ"; // Z is the bad apple
    uint8_t bytes[BYTES_SIZE];
    size_t byte_count;
    hex_error error;

    error = hex_decode(string, bytes, BYTES_SIZE, &byte_count);

    TEST_ASSERT(
        error, ==, hex_error_invalid_input,
        "Invalid character did not cause decode failure.");
}

/* Test failure on odd length input string. */
void test_odd_length() {
    const char *string = "ABCDE"; // E is the bad apple
    uint8_t bytes[BYTES_SIZE];
    size_t byte_count;
    hex_error error;

    error = hex_decode(string, bytes, BYTES_SIZE, &byte_count);

    TEST_ASSERT(
        error, ==, hex_error_invalid_input,
        "Invalid character did not cause decode failure.");
}

/* Test failure on attempt to overfill output buffer */
void test_output_overflow() {
    const char *string = "54657374696E67313233"; // decodes to 10 characters
    uint8_t bytes[BYTES_SIZE];
    size_t byte_count;
    hex_error error;

    error = hex_decode(string, bytes, 4, &byte_count);

    TEST_ASSERT(
        error, ==, hex_error_output_full,
        "Output buffer overflow not detected.");
}

int main(void) {
    test_all_cases(test_cases, sizeof(test_cases)/sizeof(test_cases[0]));
    test_case_insensitivity();
    test_invalid_character();
    test_odd_length();
    test_output_overflow();

    return 0;
}

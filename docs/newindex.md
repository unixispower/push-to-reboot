---
layout: home
---

A controllable power strip that utilizes off-the-shelf analog two-way radios
for long range communication. This documentation intends to cover everything
necesary to build and control a PTR device.

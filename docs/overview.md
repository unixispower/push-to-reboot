---
layout: page
index: 0
title: Overview
description: Description of network and message protocol
---

# Overview
Push to reboot (PTR) is a controllable power strip that utilizes off-the-shelf
analog two-way radios for long range communication. A PTR network consists of a
"controller" radio with a keypad capable of sending DTMF tones and up to 9
individually addressable "node" devices that each have 4 controllable AC
outlets called "ports".

![Network diagram](images/overview/network.png)


## Protocol
All communication within a network occurs on a single simplex frequency. Nodes
are controlled by broadcasting DTMF "messages" from a controller radio. Each
message begins with a `*` character and ends with a `#` character; message
length refers to the payload between these delimiters. The PTR protocol has 2
message types: authentication and command. An authentication message is sent to
unlock all nodes for a period of time. A command message follows an
authentication message and specifies an action to be performed on one or all
ports on one or all nodes. After completing each step of a command a node may
reply to the controller with a confirmation tone.

![Sequence diagram](images/overview/sequence.png)

Authentication messages are dynamic and must be entered by hand on the front
keypad of a controller radio, but command messages are static and short enough
to be placed in the [PTT-ID](https://en.wikipedia.org/wiki/PTT_ID) field of a
channel. The abuse of PTT-ID to send command messages was the inspiration for
this project and the origin of the name "Push to Reboot".

### Authentication Messages
An authentication message is a 6 digit [HOTP](
https://en.wikipedia.org/wiki/HMAC-based_One-time_Password_algorithm) code
that can be generated using any [RFC 4226](https://tools.ietf.org/html/rfc4226)
compatible generator (like Google Authenticator). An authentication message
must be sent before every command message.

### Command Messages
A command message is a 3 character string in the format `<node><port><action>`
that is used to set the state of ports on a node. A command can be addressed to
a single node or to all nodes in a network. The following are descriptions of
command fields:

| Field  | Range | Description                                                |
|--------|-------|------------------------------------------------------------|
| node   | 0-9   | Destination node that command is for. 0 for all nodes.     |
| port   | 0-4   | Destination port that action is for. 0 for all ports.      |
| action | A-D   | Action to perform on port.                                 |

Action letters are assigned verbs to make entering commands by hand easier. The
following are descriptions of each action:

| Action | Verb       | Description                                           |
|--------|------------|-------------------------------------------------------|
| A      | Activate   | Turn the port(s) on.                                  |
| B      | (re)Boot   | Turn the port(s) off, delay, then turn the port(s) on.|
| C      | Call       | Turn the port(s) on, delay, then turn the port(s) off.|
| D      | Deactivate | Turn the port(s) off.                                 |

### Confirmation Tones
A confirmation tone is a beep sent from node to controller to verify that
a command was received and acted upon. No confirmation tone is sent if a
command is sent to all nodes -- doing so would cause all the nodes to broadcast
on the same frequency simultaneously. Reboot (B) and call (C) actions are long
running and utilize 2 confirmation tones: one after the action is started and
one after the action is completed. No messages can be accepted by a node while
it is acting on a command.

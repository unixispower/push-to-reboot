/*
 * PTR protocol command structure and functions.
 */

#include "command.h"


/*
 * Indicate if a character is a digit.
 *
 * character: character to test
 * return: true if `character` is a digit, false otherwise
 */ 
bool is_digit(char character) {
    return character >= '0' && character <= '9';
}

/*
 * Indicate if a character is a letter in the range A-D.
 *
 * character: character to test
 * return: true if `character` is an action, false otherwise
 */
bool is_action(char character) {
    return character >= 'A' && character <= 'D';
}

/* Verify a DTMF command string is valid and extract fields. */
bool command_parse(command_info *command, const char *command_string) {
    char node_char;
    char port_char;
    char action;

    node_char = command_string[0];
    port_char = command_string[1];
    action = command_string[2];

    if (!is_digit(node_char) || !is_digit(port_char) || !is_action(action)) {
        return false;
    }

    command->node_number = node_char - '0';
    command->port_number = port_char - '0';
    command->action = action;

    return true;
}

/* Indicate if command destination is all nodes. */
bool command_is_for_all_nodes(command_info *command) {
    return command->node_number == 0;
}

/* Indicate if command destination is all ports. */
bool command_is_for_all_ports(command_info *command) {
    return command->port_number == 0;
}

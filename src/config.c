/*
 * Device configuration structure and command processor.
 */

#include "base32.h"
#include "config.h"
#include "firmware.h"
#include "hex.h"
#include "serial.h"

#include <avr/pgmspace.h>

#include <stdlib.h>
#include <string.h>

/*
 * Command line prompt.
 */
#define PROMPT "> "

/* 
 * Number of arguments (including command) present in longest command.
 */
#define MAX_ARGUMENTS 3

/*
 * Minimum length of a secret in octets.
 */
#define MIN_SECRET_LENGTH 16


/*
 * Split a space-delimited string into tokens.
 * The input `string` is searched and a pointer to the beggining of each found
 * token is added to `tokens`. A NUL character is placed in `string` at the end
 * of each token. All tokens in `string` are counted, but only `max_count` of
 * token pointers are added to `tokens`.
 *
 * string: string containing space delimited tokens
 * tokens: output for found tokens
 * max_count: maximum count of tokens to add to `tokens`.
 * return: count of tokens in `string`
 */
size_t split(char *string, char **tokens, size_t max_count) {
    size_t count = 0;
    char *token;

    token = strtok(string, " ");
    while (token != NULL) {
        if (count < max_count) {
            tokens[count] = token;
        }
        ++count;
        token = strtok(NULL, " ");
    }

    return count;
}

/*
 * Write a help message to serial.
 */
void print_help(void) {
    serial_write_p_line(PSTR(FIRMWARE_TITLE " " FIRMWARE_VERSION));
    serial_write_p_line(PSTR("This command-line interface allows configuration of all device parameters."));  
    serial_write_newline();
    serial_write_p_line(PSTR("Commands:"));
    serial_write_p_line(PSTR("    get <setting>          Get the value of a setting"));
    serial_write_p_line(PSTR("    get all                Get the value of all settings"));
    serial_write_p_line(PSTR("    set <setting> <value>  Set the value of a setting"));
    serial_write_p_line(PSTR("    reset                  Set all values to default"));
    serial_write_p_line(PSTR("    version                Show firmware version information"));
    serial_write_p_line(PSTR("    help                   Show this help message"));
    serial_write_p_line(PSTR("Settings:"));
    serial_write_p_line(PSTR("    node_number            Node number (1-9)"));
    serial_write_p_line(PSTR("    reboot_duration        Reboot duration in seconds (1-255)"));
    serial_write_p_line(PSTR("    call_duration          Call duration in seconds (1-255)"));
    serial_write_p_line(PSTR("    auth_duration          Authentication duration in seconds (1-255)"));
    serial_write_p_line(PSTR("    auth_secret.b32        Authentication secret encoded as base 32"));
    serial_write_p_line(PSTR("    auth_secret.hex        Authentication secret encoded as hexadecimal"));
    serial_write_newline();
    serial_write_p_line(PSTR("A secret MUST be set using \"set auth_secret.b32\" or \"set auth_secret.hex\""));
    serial_write_p_line(PSTR("otherwise all DTMF messages will be ignored."));
}

/*
 * Write a version message to serial.
 */
void print_version(void) {
    serial_write_p_line(PSTR(FIRMWARE_TITLE " " FIRMWARE_VERSION));
    serial_write_p_line(PSTR("Copyright (c) " FIRMWARE_COPYRIGHT));
    serial_write_p_line(PSTR("Released under " FIRMWARE_LICENSE " license"));
}

/*
 * Write the base 10 representation of a uint16_t to serial.
 *
 * value: the number to format and print
 */
void print_uint16(uint16_t value) {
    char string[6]; // 5 digits max + NUL

    itoa(value, string, 10);
    serial_write_string(string);
}

/*
 * Write the status of the secret to serial.
 * Only an indicator of wheter the secret is set is written, never the actual
 * secret.
 *
 * config: configuration containing authentication context
 */
void print_secret(config_info *config) {
    if (config_is_secret_set(config)) {
        serial_write_p_string(PSTR("**********"));
    } else {
        serial_write_p_string(PSTR("[UNSET]"));
    }
}

/*
 * Write all configurable fields of the config to serial.
 *
 * config: config to print
 */
void print_config(config_info *config) {
    // node
    serial_write_p_string(PSTR("node_number:     "));
    print_uint16(config->node_number);
    serial_write_newline();
    // reboot duration
    serial_write_p_string(PSTR("reboot_duration: "));
    print_uint16(config->reboot_duration);
    serial_write_newline();
    // call duration
    serial_write_p_string(PSTR("call_duration:   "));
    print_uint16(config->call_duration);
    serial_write_newline();
    // auth duration
    serial_write_p_string(PSTR("auth_duration:   "));
    print_uint16(config->auth_duration);
    serial_write_newline();
    // auth secret
    serial_write_p_string(PSTR("auth_secret:     "));
    print_secret(config);
    serial_write_newline();
}

/*
 * Parse a uint8_t from a base 10 string representation.
 * The parsed value is only accepted if it is within the range `min` to `max`.
 * Errors are writen to serial.
 *
 * string: string containing a formatted number
 * name: name of setting to print in error message
 * min: minimum acceptable value
 * max: maximum acceptable value
 * value: output for parsed number
 * return: true if the number was parsed and is within range, false otherwise
 */
bool parse_uint8(
        char *string, char *name, uint8_t min, uint8_t max, uint8_t *value) {
    int raw_value;

    raw_value = atoi(string);
    if (raw_value < min || raw_value > max) {
        // print error message
        serial_write_p_string(PSTR("Value of \""));
        serial_write_string(name);
        serial_write_p_string(PSTR("\" must be in range "));  
        print_uint16(min);
        serial_write('-');
        print_uint16(max);
        serial_write_line(".");

        return false;
    }
    *value = raw_value;

    return true;
}

/*
 * Parse an HOTP secret from a base 32 encoded string.
 *
 * context: HOTP context to initialize on success
 * string: base 32 representation of secret bits
 */
bool parse_base32_secret(hotp_context *context, char *string) {
    uint8_t secret[HOTP_SECRET_SIZE];
    size_t secret_length;
    base32_error error;

    error = base32_decode(string, secret, HOTP_SECRET_SIZE, &secret_length);
    // secret is not base32
    if (error == base32_error_invalid_input) {
        serial_write_p_line(PSTR("Invalid base 32 string."));

        return false;
    }
    // secret is too long
    if (error == base32_error_output_full) {
        serial_write_p_string(PSTR("Secret must be at most "));
        print_uint16(HOTP_SECRET_SIZE * 8);
        serial_write_p_line(PSTR(" bits long."));

        return false;
    }
    // secret too short
    if (secret_length < MIN_SECRET_LENGTH) {
        serial_write_p_string(PSTR("Secret must be at least "));
        print_uint16(MIN_SECRET_LENGTH * 8);
        serial_write_p_line(PSTR(" bits long."));

        return false;
    }

    hotp_init(context, secret, secret_length);

    return true;
}

/*
 * Parse an HOTP secret from a base 32 encoded string.
 *
 * context: HOTP context to initialize on success
 * string: base 32 representation of secret bits
 */
bool parse_hex_secret(hotp_context *context, char *string) {
    uint8_t secret[HOTP_SECRET_SIZE];
    size_t secret_length;
    hex_error error;

    error = hex_decode(string, secret, HOTP_SECRET_SIZE, &secret_length);
    // secret is not hex
    if (error == hex_error_invalid_input) {
        serial_write_p_line(PSTR("Invalid hexadecimal string."));

        return false;
    }
    // secret is too long
    if (error == hex_error_output_full) {
        serial_write_p_string(PSTR("Secret must be at most "));
        print_uint16(HOTP_SECRET_SIZE * 8);
        serial_write_p_line(PSTR(" bits long."));

        return false;
    }
    // secret too short
    if (secret_length < MIN_SECRET_LENGTH) {
        serial_write_p_string(PSTR("Secret must be at least "));
        print_uint16(MIN_SECRET_LENGTH * 8);
        serial_write_p_line(PSTR(" bits long."));

        return false;
    }

    hotp_init(context, secret, secret_length);

    return true;
}

/*
 * Print the value of a configuration setting to serial.
 * Error handling and reporting is handled by this function.
 *
 * config: configuration values
 * argc: count of elements in `argv`
 * argv: list of command and argument strings
 */ 
bool exec_get(config_info *config, size_t argc, char **argv) {
    char *setting;

    if (argc != 2) {
        serial_write_p_line(PSTR("Command \"get\" requires 1 argument."));

        return false;
    }

    setting = argv[1];
    // all settings
    if (strcmp(setting, "all") == 0) {
        print_config(config);
    // node number
    } else if (strcmp_P(setting, PSTR("node_number")) == 0) {
        print_uint16(config->node_number);
        serial_write_newline();
    // reboot duration
    } else if (strcmp_P(setting, PSTR("reboot_duration")) == 0) {
        print_uint16(config->reboot_duration);
        serial_write_newline();
    // call duration
    } else if (strcmp_P(setting, PSTR("call_duration")) == 0) {
        print_uint16(config->call_duration);
        serial_write_newline();
    // auth duration
    } else if (strcmp_P(setting, PSTR("auth_duration")) == 0) {
        print_uint16(config->auth_duration);
        serial_write_newline();
    // auth secret
    } else if (strcmp_P(setting, PSTR("auth_secret")) == 0
            || strcmp_P(setting, PSTR("auth_secret.b32")) == 0
            || strcmp_P(setting, PSTR("auth_secret.hex")) == 0) {
        print_secret(config);
        serial_write_newline();
    // unknown setting
    } else {
        serial_write_p_string(PSTR("Unknown setting \""));
        serial_write_string(setting);
        serial_write_p_line(PSTR("\"."));

        return false;
    }

    return true;
}

/*
 * Set the value of a configuration setting.
 * Error handling and reporting is handled by this function.
 *
 * config: configuration values
 * argc: count of elements in `argv`
 * argv: list of command and argument strings
 */ 
bool exec_set(config_info *config, size_t argc, char **argv) {
    char *setting;
    char *value;  

    if (argc != 3) {
        serial_write_p_line(PSTR("Command \"set\" requires 2 arguments."));

        return false;
    }

    setting = argv[1];
    value = argv[2];        
    // node number
    if (strcmp_P(setting, PSTR("node_number")) == 0) {
        if (!parse_uint8(value, setting, 1, 9, &(config->node_number))) {
            return false;
        }
    // reboot duration
    } else if (strcmp_P(setting, PSTR("reboot_duration")) == 0) {
        if (!parse_uint8(value, setting, 1, 255, &(config->reboot_duration))) {
            return false;
        }
    // call duration
    } else if (strcmp_P(setting, PSTR("call_duration")) == 0) {
        if (!parse_uint8(value, setting, 1, 255, &(config->call_duration))) {
            return false;
        }
    // auth duration
    } else if (strcmp_P(setting, PSTR("auth_duration")) == 0) {
        if (!parse_uint8(value, setting, 1, 255, &(config->auth_duration))) {
            return false;
        }
    // auth secret (alias)
    } else if (strcmp_P(setting, PSTR("auth_secret")) == 0) {
        serial_write_p_line(
            PSTR("Cannot set \"auth_secret\" without knowing encoding."));
        serial_write_p_line(
            PSTR("Use \"set auth_secret.b32\" or \"set auth_secret.hex\"."));

        return false;
    // auth secret (base 32)
    } else if (strcmp_P(setting, PSTR("auth_secret.b32")) == 0) {
        if (!parse_base32_secret(&(config->auth_context), value)) {
            return false;
        }
    // auth secret (hex)
    } else if (strcmp_P(setting, PSTR("auth_secret.hex")) == 0) {
        if (!parse_hex_secret(&(config->auth_context), value)) {
            return false;
        }
    // unknown setting
    } else {
        serial_write_p_string(PSTR("Unknown setting \""));
        serial_write_string(setting);
        serial_write_p_line(PSTR("\"."));

        return false;
    }

    return true;
}

/* Write a command prompt to serial. */
void config_prompt() {
    serial_write_p_string(PSTR("> "));
}

/* Execute a configuration command. */
bool config_exec(config_info *config, char *command_string) {
    // parse arguments into array of strings
    char *argv[MAX_ARGUMENTS];
    size_t argc;
    char *command;

    argc = split(command_string, argv, MAX_ARGUMENTS);

    // discard empty command
    if (argc == 0) {
        return false;
    }

    command = argv[0];
    // help
    if (strcmp_P(command, PSTR("help")) == 0) {
        print_help();

        return true;
    // version
    } else if (strcmp_P(command, PSTR("version")) == 0) {
        print_version();

        return true;
    // get
    } else if (strcmp_P(command, PSTR("get")) == 0) {
        return exec_get(config, argc, argv);
    // set
    } else if (strcmp_P(command, PSTR("set")) == 0) {
        return exec_set(config, argc, argv);
    // reset
    } else if (strcmp_P(command, PSTR("reset")) == 0) {
        *config = (config_info)CONFIG_DEFAULT_VALUES;
        print_config(config); // show new values

        return true;
    // unknown command
    } else {
        serial_write_p_string(PSTR("Unknown command \""));
        serial_write_string(command);
        serial_write_p_line(PSTR("\". Enter \"help\" to show commands."));

        return false;
    }
}

/* Indicate if the authentication secret has been set. */
bool config_is_secret_set(config_info *config) {
    return config->auth_context.secret_length != 0;
}

/*
 * Driver for LC Technology MT8870 DTMF decoder board.
 * This module is used to read delimited DTMF message strings. Messages that
 * do not contain delimiters are ignored, see `dtmf_read_line()` for details.
 */

#ifndef DTMF_H
#define DTMF_H

#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>


/*
 * Set direction of pins and initialize pull-ups.
 * This function needs to be called before any other in the same module.
 */
void dtmf_init(void);

/*
 * Indicate if a character is available to be read.
 *
 * return: true if a character is available, false otherwise
 */
bool dtmf_available(void);

/*
 * Read a single character from the DTMF decoder.
 * This function requires the timer module `timer.h` to be initialized.
 *
 * timeout: number of milliseconds to wait for an incoming character
 * return: the read character or NUL on timeout
 */
char dtmf_read(uint32_t timeout);

/*
 * Read a delimited string of characters from the DTMF decoder.
 * DTMF strings must start with '*' and end with '#'; these characters will be
 * omitted from the buffer. If subsequent '*' characters are recieved the
 * buffer will be cleared and the input loop will restart. Timeout or overflow
 * will cause the input string to be discarded and the function to return 0.
 * This function requires the timer module `timer.h` to be initialized.
 *
 * buffer: output buffer for recieved character string
 * length: length of the output buffer
 * timeout: number of milliseconds to wait for an incoming character
 * return: the count of read characters; 0 on timeout or overflow
 */
size_t dtmf_read_line(char *buffer, size_t length, uint32_t timeout);


#endif /* DTMF_H */

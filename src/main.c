/*
 * Push to Reboot firmware.
 */

#include "command.h"
#include "config.h"
#include "dtmf.h"
#include "hotp.h"
#include "relay.h"
#include "serial.h"
#include "timer.h"

#include <avr/eeprom.h>

#include <stdbool.h>
#include <stdint.h>
#include <string.h>

/*
 * Size of buffer that accepts serial and DTMF input.
 */
#define INPUT_BUFFER_SIZE 128

/*
 * Duration to wait for serial or DTMF input in milliseconds.
 */
#define INPUT_TIMEOUT 10000


/*
 * Location of configuration in EEPROM.
 */
config_info EEMEM eeprom_config = CONFIG_DEFAULT_VALUES;

/*
 * Indicate if an authentication window has elapsed.
 *
 * config: configuration containing authentication settings
 * auth_time: timestamp of most recent authentication
 */
bool auth_is_expired(config_info *config, uint32_t auth_time) {
    uint32_t auth_duration;
    bool is_expired;

    auth_duration = (uint32_t)config->auth_duration * 1000U;
    is_expired = (timer_uptime() - auth_time) >= auth_duration;

    return is_expired;
}

/*
 * Execute a DTMF command.
 *
 * config: configuration containing timing settings
 * command_string: string of DTMF digits
 */
void command_exec(config_info *config, char *command_string) {
    command_info command;
    uint8_t relay_mask;

    // discard malformed command
    if (!command_parse(&command, command_string)) {
        return;
    }
    // discard command if not for this node
    if (command.node_number != config->node_number
            && !command_is_for_all_nodes(&command)) {
        return;
    }
    // discard command if not for an available port
    if (command.port_number > RELAY_COUNT) {
        return;
    }

    relay_mask = command_is_for_all_ports(&command) ?
        RELAY_MASK_ALL: relay_select(command.port_number);
    switch (command.action) {
        // activate
        case COMMAND_ACTION_ACTIVATE:
            relay_set(relay_mask);
            break;
        // reboot
        case COMMAND_ACTION_REBOOT:
            relay_clear(relay_mask);
            timer_delay_secs(config->reboot_duration);
            relay_set(relay_mask);
            break;
        // call
        case COMMAND_ACTION_CALL:
            relay_set(relay_mask);
            timer_delay_secs(config->call_duration);
            relay_clear(relay_mask);
            break;
        // deactivate
        case COMMAND_ACTION_DEACTIVATE:
            relay_clear(relay_mask);
            break;
    }

    // save port state
    config->port_state = relay_read();
}

int main(void) {
    config_info config;
    char input_buffer[INPUT_BUFFER_SIZE];
    bool auth_is_valid = false;
    uint32_t auth_time = 0;

    // initialize hardware
    timer_init();
    serial_init();
    dtmf_init();
    relay_init();

    // load saved state
    eeprom_read_block(&config, &eeprom_config, sizeof(config_info));
    relay_write(config.port_state);

    // show initial prompt
    serial_write_newline();
    config_prompt();

    while (true) {
        // handle serial configuration command
        if (serial_available()) {
            serial_read_line(input_buffer, INPUT_BUFFER_SIZE, INPUT_TIMEOUT);
            if (config_exec(&config, input_buffer)) {
                relay_write(config.port_state); // set state in case of reset
                eeprom_update_block(
                    &config, &eeprom_config, sizeof(config_info));
            }
            config_prompt();
        }

        // handle incoming DTMF message
        if (dtmf_available()) {
            size_t length = dtmf_read_line(
                input_buffer, INPUT_BUFFER_SIZE, INPUT_TIMEOUT);

            // authentication
            if (length == HOTP_PASSWORD_LENGTH) {
                // ignore authentication if secret is unset
                if (!config_is_secret_set(&config)) {
                    continue;
                }

                // check password
                auth_is_valid = hotp_check(&(config.auth_context), input_buffer);
                if (auth_is_valid) {
                    auth_time = timer_uptime();
                }
            // command
            } else if (length == COMMAND_MESSAGE_LENGTH) {
                // reject command if not authenticated
                if (!auth_is_valid || auth_is_expired(&config, auth_time)) {
                    continue;
                }

                command_exec(&config, input_buffer);
                eeprom_update_block(
                    &config, &eeprom_config, sizeof(config_info));
                auth_is_valid = false; // end authentication window
            }
        }

        // mark authentication as invalid immediately after expiration
        if (auth_is_valid) {
            auth_is_valid = !auth_is_expired(&config, auth_time);
        }
    }

    return 0;
}

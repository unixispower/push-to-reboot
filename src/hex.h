/*
 * Hexadecimal (base 16) decoder.
 * This implementation is based on RFC 4648 and uses the original alphabet
 * specified therein. See https://tools.ietf.org/html/rfc4648 for details.
 *
 * To decode a hexadecimal string:
 *
 *    #define BYTES_SIZE 32
 *
 *    const char *string = "54657374696E67313233";
 *    uint8_t bytes[BYTES_SIZE];
 *    size_t byte_count;
 *    hex_error error;
 *
 *    error = hex_decode(string, bytes, BYTES_SIZE, &byte_count);
 */

#ifndef HEX_H
#define HEX_H

#include <stddef.h>
#include <stdint.h>


/*
 * Error codes returned by hex functions.
 */
typedef enum hex_error {
    hex_error_success = 0,
    hex_error_invalid_input, // invalid input string
    hex_error_output_full,   // output buffer cannot hold all decoded data
} hex_error;

/*
 * Decode a hexadecimal string to an array of bytes.
 * Input characters may be uppercase or lowercase. Attempting to decode a
 * string that contains illegal characters or a string with an odd character
 * length will result in `hex_error_invalid_input` being returned. Attempting
 * to write more than `bytes_length` characters to `bytes` will result in
 * `hex_error_output_full` being returned. 
 *
 * string: null delimited hexadecimal encoded string
 * bytes: destination for decoded bytes
 * bytes_length: length of `bytes` array
 * byte_count: count of bytes successfully decoded
 * return: error indicating if the decode was successful
 */
hex_error hex_decode(
        const char *string, uint8_t *bytes, size_t bytes_length,
        size_t *byte_count);


#endif /* HEX_H */

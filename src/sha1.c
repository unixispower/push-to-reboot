/*
 * Memory efficient SHA1 implementation.
 */

#include "sha1.h"

#include <string.h>

/*
 * Shift function defined by SHA1.
 */
#define SHA1_SHIFT(BITS, WORD) (((WORD) << (BITS)) | ((WORD) >> (32 - (BITS))))


/* Process a 512 bit block. */
void process_block(sha1_context *context) {
    uint32_t w[16];
    uint32_t a, b, c, d, e;

    a = context->intermediate[0];
    b = context->intermediate[1];
    c = context->intermediate[2];
    d = context->intermediate[3];
    e = context->intermediate[4];

    for(size_t t = 0; t < 16; t++) {
        w[t]  = (uint32_t)(context->block[t * 4]) << 24;
        w[t] |= (uint32_t)(context->block[t * 4 + 1]) << 16;
        w[t] |= (uint32_t)(context->block[t * 4 + 2]) << 8;
        w[t] |= (uint32_t)(context->block[t * 4 + 3]);
    }

    for(size_t t = 0; t < 80; ++t) {
        size_t s;
        uint32_t temp;

        s = t & 0x0F;

        if (t >= 16) {
            w[s] = SHA1_SHIFT(1,
                w[(s + 13) & 0x0F] ^ w[(s + 8) & 0x0F] ^ w[(s + 2) & 0x0F] ^ w[s]);
        }

        if (t < 20) {
            temp = SHA1_SHIFT(5, a)
                + ((b & c) | ((~b) & d)) + e + w[s] + 0x5A827999;
        } else if (t < 40) {
            temp = SHA1_SHIFT(5, a) + (b ^ c ^ d) + e + w[s] + 0x6ED9EBA1;
        } else if (t < 60) {
            temp = SHA1_SHIFT(5, a)
                + ((b & c) | (b & d) | (c & d)) + e + w[s] + 0x8F1BBCDC;
        } else {
            temp = SHA1_SHIFT(5, a) + (b ^ c ^ d) + e + w[s] + 0xCA62C1D6;
        }

        e = d;
        d = c;
        c = SHA1_SHIFT(30, b);
        b = a;
        a = temp;
    }

    context->intermediate[0] += a;
    context->intermediate[1] += b;
    context->intermediate[2] += c;
    context->intermediate[3] += d;
    context->intermediate[4] += e;

    context->block_index = 0;
}

/* Set the initial state of a SHA1 context. */
void sha1_init(sha1_context *context) {
    context->intermediate[0] = 0x67452301;
    context->intermediate[1] = 0xEFCDAB89;
    context->intermediate[2] = 0x98BADCFE;
    context->intermediate[3] = 0x10325476;
    context->intermediate[4] = 0xC3D2E1F0;

    context->block_index = 0;
    context->bit_length = 0;
}

/* Update a SHA1 context with a message. */
void sha1_update(sha1_context *context, const uint8_t *message, size_t length) {
    // process message one block at a time
    while(length--) {
        context->block[context->block_index++] = *message++;

        // process block once full
        if (context->block_index == SHA1_BLOCK_SIZE) {
            process_block(context);
        }

        context->bit_length += 8;
    }
}

/* Calculate the digest of all messages added to a context. */
void sha1_digest(sha1_context *context, uint8_t *digest) {
    // remainder of block too small to hold length
    if (context->block_index > 55) {
        // pad block to end, length goes in next block
        context->block[context->block_index++] = 0x80;
        while(context->block_index < SHA1_BLOCK_SIZE) {
            context->block[context->block_index++] = 0;
        }

        // process padded block
        process_block(context);

        // pad the beggining of the next block up to the length area
        while(context->block_index < 56) {
            context->block[context->block_index++] = 0;
        }
    // remainder of block big enough to hold length
    } else {
        // pad the block up to the length area
        context->block[context->block_index++] = 0x80;
        while(context->block_index < 56) {
            context->block[context->block_index++] = 0;
        }
    }

    // copy length to block high byte first
    for (size_t i = 0; i < sizeof(context->bit_length); ++i) {
        size_t ri; // 15..0

        ri = sizeof(context->bit_length) - 1 - i;
        context->block[56 + i] = context->bit_length >> (ri * 8);
    }

    // process final block
    process_block(context);

    // clear sensitive data from block memory
    memset(context->block, 0, SHA1_BLOCK_SIZE);

    // write hash
    for (size_t i = 0; i < SHA1_HASH_SIZE; ++i) {
        digest[i] = context->intermediate[i >> 2] >> 8 * (3 - (i & 0x03));
    }
}

/* Calculate the SHA1 hash of a message. */
void sha1_hash(const uint8_t *message, size_t length, uint8_t *digest) {
    sha1_context context;

    sha1_init(&context);
    sha1_update(&context, message, length);
    sha1_digest(&context, digest);
}

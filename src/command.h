/*
 * PTR protocol command structure and functions.
 *
 * To validate and parse a command:
 *
 *     const char *string = "11A";
 *     command_info command;
 *     bool success;
 *
 *     success = command_parse(&command, string);
 */

#ifndef COMMAND_H
#define COMMAND_H

#include <stdbool.h>
#include <stdint.h>

/*
 * Length of a command message in characters
 */
#define COMMAND_MESSAGE_LENGTH 3


/*
 * Actions that can be performed on ports.
 */
typedef enum command_action {
    COMMAND_ACTION_ACTIVATE   = 'A', // turn port on
    COMMAND_ACTION_REBOOT     = 'B', // turn port off then on
    COMMAND_ACTION_CALL       = 'C', // turn port on then off
    COMMAND_ACTION_DEACTIVATE = 'D'  // turn port off
} command_action;

/*
 * Fields of a 3-digit DTMF command.
 */
typedef struct command_info {
    uint8_t node_number;    // node number that command is destined for
    uint8_t port_number;    // port number that action applies to
    command_action action;  // action to be performed on the port
} command_info;

/*
 * Verify a DTMF command string is valid and extract fields.
 *
 * command: destination for extracted fields
 * command_string: command string to be verified and parsed
 * return: true if command is valid and was parsed, false otherwise
 */
bool command_parse(command_info *command, const char *command_string);

/*
 * Indicate if command destination is all nodes.
 *
 * command: command to inspect
 * return: true if command destination is all nodes, false otherwise
 */
bool command_is_for_all_nodes(command_info *command);

/*
 * Indicate if command destination is all ports.
 *
 * command: command to inspect
 * return: true if command destination is all ports, false otherwise
 */
bool command_is_for_all_ports(command_info *command);


#endif /* COMMAND_H */

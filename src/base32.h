/*
 * Base 32 decoder.
 * This implementation is based on RFC 4648 and uses the original alphabet
 * specified therein. See https://tools.ietf.org/html/rfc4648 for details.
 *
 * To decode a base 32 string:
 *
 *    #define BYTES_SIZE 32
 *
 *    const char *string = "KRSXG5DJNZTTCMRT";
 *    uint8_t bytes[BYTES_SIZE];
 *    size_t byte_count;
 *    base32_error error;
 *
 *    error = base32_decode(string, bytes, BYTES_SIZE, &byte_count);
 */

#ifndef BASE32_H
#define BASE32_H

#include <stddef.h>
#include <stdint.h>


/*
 * Error codes returned by base32 functions.
 */
typedef enum base32_error {
    base32_error_success = 0,
    base32_error_invalid_input, // invalid input string
    base32_error_output_full,   // output buffer cannot hold all decoded data
} base32_error;

/*
 * Decode a base 32 string to an array of bytes.
 * Input characters may be uppercase or lowercase. Padding characters are
 * optional. Attempting to decode a string that contains illegal characters
 * will result in `base32_error_invalid_input` being returned. Attempting to
 * write more than `bytes_length` characters to `bytes` will result in
 * `base32_error_output_full` being returned.
 *
 * string: null delimited base 32 encoded string
 * bytes: destination for decoded bytes
 * bytes_length: length of `bytes` array
 * byte_count: count of bytes successfully decoded
 * return: error indicating if the decode was successful
 */
base32_error base32_decode(
        const char *string, uint8_t *bytes, size_t bytes_length,
        size_t *byte_count);


#endif /* BASE32_H */
